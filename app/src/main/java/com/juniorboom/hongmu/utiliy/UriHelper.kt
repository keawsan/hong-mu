@file:Suppress("DEPRECATION")

package com.juniorboom.hongmu.utiliy

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.OpenableColumns
import androidx.core.net.toUri
import com.jozzee.android.core.BuildConfig
import com.jozzee.android.core.simpleName
import com.jozzee.android.core.utility.Logger
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*

fun Uri.getRealPath(context: Context?): RealUri? {
    if (context == null) {
        return null
    }
    //Logger.error("Uri Extension", "Path: $path")
    //Logger.error("Uri Extension", "Scheme: $scheme")
    //Logger.error("Uri Extension", "Authority: $authority")
    //Logger.error("Uri Extension", ":${toString()}")

    //DocumentProvider
    if (DocumentsContract.isDocumentUri(context, this)) {
        //ExternalStorageProvider
        if (isExternalStorageDocument()) {
            val docIdSet = DocumentsContract.getDocumentId(this).split(":")
            if (docIdSet[0].toLowerCase(Locale.US) == "primary") {
                return RealUri(
                    "${Environment.getExternalStorageDirectory()}/${docIdSet[1]}",
                    false
                )
            } else if (authority!!.contains("com.android.externalstorage.documents")) {
                return RealUri(
                    "/storage/${docIdSet[0]}/${docIdSet[1]}",
                    false
                )
            }
        }
        //DownloadsProvider
        else if (isDownloadsDocument()) {
            val id: String = DocumentsContract.getDocumentId(this)
            if (id.startsWith("raw:")) {
                return RealUri(
                    id.substring(4),
                    false
                )
            }

            val contentUriPrefixesToTry = arrayOf("content://downloads/public_downloads",
                    "content://downloads/my_downloads",
                    "content://downloads/all_downloads")

            contentUriPrefixesToTry.forEach { contentUriPrefix ->
                val contentUri = ContentUris.withAppendedId(Uri.parse(contentUriPrefix), id.toLong())
                try {
                    val path: String = contentUri.getDataColumn(context, null, null)
                    if (path.isNotBlank()) {
                        return RealUri(path, false)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            //path could not be retrieved using ContentResolver, therefore copy file to accessible cache using streams
            val fileName = getFileName(context)
            val cacheDir =
                getDocumentCacheDir(context)
            val file: File? =
                generateFileName(fileName, cacheDir)
            //var destinationPath: String?
            if (file != null) {
                saveFileFromUri(context, file.absolutePath)
                return RealUri(
                    file.absolutePath,
                    false
                )
            }
        }
        //MediaProvider
        else if (isMediaDocument()) {
            val docIdSet = DocumentsContract.getDocumentId(this).split(":")
            val contentUri = when (docIdSet[0]) {
                "image" -> MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                "video" -> MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                "audio" -> MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                else -> null
            }
            val selection = "_id=?"
            val selectionArgs = arrayOf(docIdSet[1])
            return RealUri(
                contentUri?.getDataColumn(context, selection, selectionArgs)
                    ?: "", false
            )
        }
    }
    //MediaStore (and general)
    if (scheme?.toLowerCase(Locale.US) == "content") {
        if (isGooglePhotosUri()) {
            return RealUri(
                lastPathSegment ?: "",
                false
            )
        } else if (isGoogleDrive()) {
            Logger.debug(simpleName(), "Is Google Drive")
            //val docFile = DocumentFile.fromSingleUri(context, this)
            //Logger.warning(simpleName(), "Name: ${docFile?.name}")
            //Logger.warning(simpleName(), "Uri: ${docFile?.uri}")

            var cursor: Cursor? = null

            try {
                cursor = context.contentResolver.query(this, null, null, null, null)
                if (cursor != null) {
                    val nameIndex: Int = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
                    val sizeIndex: Int = cursor.getColumnIndex(OpenableColumns.SIZE)
                    cursor.moveToFirst()
                    val name: String = cursor.getString(nameIndex)
                    val size: Long = cursor.getLong(sizeIndex)

                    Logger.error(simpleName(), "Name: $name")
                    Logger.error(simpleName(), "Size: $size")

                    //Create Temp file
                    val time = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Date())
                    val fileName = ("/IMG_$time.jpg")

                    val directory = File("${Environment.getExternalStorageDirectory()}/CP Goody").also {
                        if (it.exists().not()) {
                            it.mkdirs()
                        }
                    }

                    val tempFile = File("$directory$fileName")

                    //Copy input stream to tempFile.
                    context.contentResolver.openInputStream(this)?.use { input ->
                        FileOutputStream(tempFile).use { fileOut ->
                            input.copyTo(fileOut)
                        }
                    }

                    Logger.info(simpleName(), "Temp file: ${tempFile.toUri().path}")
                    Logger.info(simpleName(), "size: ${tempFile.length()}")

                    if (tempFile.length() > 0) {
                        return RealUri(
                            tempFile.toUri().path ?: "", true
                        )
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                cursor?.close()
            }
        } else {
            return RealUri(
                getDataColumn(
                    context,
                    null,
                    null
                ), false
            )
        }
    }
    //File
    else if (scheme?.toLowerCase(Locale.US) == "file") {
        return RealUri(path ?: "", false)
    }

    return null
}

fun Uri.getDataColumn(context: Context, selection: String?, selectionArgs: Array<String>?): String {

    var cursor: Cursor? = null
    val column: String = MediaStore.Files.FileColumns.DATA
    val projection = arrayOf(column)

    try {
        cursor = context.contentResolver.query(this, projection, selection, selectionArgs, null)
        if (cursor != null && cursor.moveToFirst()) {
            val columnIndex = cursor.getColumnIndexOrThrow(column)
            return cursor.getString(columnIndex)
        }
    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        cursor?.close()
    }
    return ""
}

fun getDocumentCacheDir(context: Context): File {
    val dir = File(context.cacheDir, "documents")
    if (dir.exists().not()) {
        dir.mkdirs()
    }
    return dir
}

fun Uri.getFileName(context: Context): String {
    var fileName = ""
    val mimeType: String? = context.contentResolver.getType(this)

    if (mimeType != null) {
        val path = toString()
        fileName = when (path.isNotBlank()) {
            true -> path.substring(path.lastIndexOf('/') + 1)
            false -> File(path).name
        }
    } else {
        val returnCursor = context.contentResolver.query(this, null, null, null, null)
        if (returnCursor != null) {
            val nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            returnCursor.moveToFirst()
            fileName = returnCursor.getString(nameIndex)
            returnCursor.close()
        }
    }
    return fileName
}

private fun generateFileName(name: String, directory: File): File? {
    var file = File(directory, name)
    if (file.exists()) {
        var fileName = name
        var extension = ""

        val dotIndex = name.lastIndexOf('.')
        if (dotIndex > 0) {
            fileName = name.substring(0, dotIndex)
            extension = name.substring(dotIndex)
        }

        var index = 0
        while (file.exists()) {
            index++
            file = File(directory, "$fileName($index)$extension")
        }
    }

    try {
        if (file.createNewFile().not()) {
            return null
        }
    } catch (e: Exception) {
        e.printStackTrace()
        return null
    }
    return file
}

fun Uri.saveFileFromUri(context: Context, destinationPath: String) {
    var ins: InputStream? = null
    var bos: BufferedOutputStream? = null
    try {
        ins = context.contentResolver.openInputStream(this)
        bos = BufferedOutputStream(FileOutputStream(destinationPath, false))
        val buf = ByteArray(1024)
        if (ins != null) {
            ins.read(buf)
        }
        if (ins != null) {
            do {
                bos.write(buf)
            } while (ins.read(buf) != -1)
        }
    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        try {
            ins?.close()
            bos?.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}

/**
 * @param uri The Uri to check.
 * @return Whether the Uri authority is local.
 */
fun Uri.isLocalStorageDocument(): Boolean = "${BuildConfig.APPLICATION_ID}.provider" == authority

fun Uri.isGoogleDrive(): Boolean = authority == "com.google.android.apps.docs.storage"
        || authority == "com.google.android.apps.docs.storage.legacy"
        || authority?.contains("com.google.android.apps") == true

fun Uri.isGooglePhotosUri(): Boolean = authority == "com.google.android.apps.photos.content"

/**
 * @param uri The Uri to check.
 * @return Whether the Uri authority is ExternalStorageProvider.
 */
fun Uri.isExternalStorageDocument(): Boolean = authority == "com.android.externalstorage.documents"

fun Uri.isDownloadsDocument(): Boolean = authority == "com.android.providers.downloads.documents"

fun Uri.isMediaDocument(): Boolean = authority == "com.android.providers.media.documents"


