package com.juniorboom.hongmu.utiliy

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import com.jozzee.android.core.utility.Logger
import java.io.ByteArrayOutputStream

open class ImageUtil {
    /**
     * [requestSize] Default size to reduce
     */
    fun reduceImageSizeBeforeUpload(imagePath: String, requestSize: Int = 640): ByteArray {
        var bitmap = BitmapFactory.decodeFile(imagePath)
        Logger.warning("Multipart", "Original Image: w: ${bitmap.width}, h: ${bitmap.height}")

        //Here's what the orientation values mean: http://sylvana.net/jpegcrop/exif_orientation.html
        val exif = ExifInterface(imagePath)
        val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1)
        if (orientation != 1) {
            val matrix = Matrix()
            when (exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1)) {
                3 -> matrix.postRotate(180f)
                6 -> matrix.postRotate(90f)
                8 -> matrix.postRotate(270f)
            }
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
            Logger.error("Multipart", "Rotate Image: w: ${bitmap.width}, h: ${bitmap.height}")
        }

        var reqWidth = 0
        var reqHeight = 0

        if (bitmap.width > requestSize || bitmap.height > requestSize) {
            if (bitmap.width >= bitmap.height) {
                reqWidth = requestSize
                reqHeight = (bitmap.height / (bitmap.width / reqWidth.toFloat())).toInt()
            } else if (bitmap.width < bitmap.height) {
                reqHeight = requestSize
                reqWidth = (bitmap.width / (bitmap.height / reqHeight.toFloat())).toInt()
            }
        } else {
            reqWidth = bitmap.width
            reqHeight = bitmap.height
        }

        Logger.info("Multipart", "New size: w: $reqWidth, h: $reqHeight")
        return Bitmap.createScaledBitmap(bitmap, reqWidth, reqHeight, false).toByteArray()
    }
    private fun Bitmap.toByteArray(): ByteArray {
        val stream = ByteArrayOutputStream()
        compress(Bitmap.CompressFormat.JPEG, 100, stream)
        val byteArray = stream.toByteArray()
        stream.reset()
        stream.close()
        return byteArray
    }
}