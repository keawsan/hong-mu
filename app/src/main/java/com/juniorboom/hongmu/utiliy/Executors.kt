package com.juniorboom.hongmu.utiliy

import androidx.fragment.app.Fragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.concurrent.Executors

private val IO_EXECUTOR = Executors.newSingleThreadExecutor()

fun currentThread(): String = Thread.currentThread().name

/**
 * Utility method to run blocks on a dedicated background thread, used for io/database work.
 */
fun runOnIoThread(f: () -> Unit) {
    IO_EXECUTOR.execute(f)
}

/**
 * executor block with coroutine scope on main thread
 *[delay] time in millisecond.
 *[block] the coroutine code.
 */
fun runOnMainThread(delay: Long = 0, block: () -> Unit) = when (delay > 0) {
    true -> CoroutineScope(Dispatchers.IO).launch {
        delay(delay)
        CoroutineScope(Dispatchers.Main).launch {
            block()
        }
    }
    false -> CoroutineScope(Dispatchers.Main).launch {
        block()
    }
}

/**
 * Utility method to run blocks on a ui thread
 */
fun Fragment.runOnUiThread(f: () -> Unit) {
    activity?.runOnUiThread { f() }
}
