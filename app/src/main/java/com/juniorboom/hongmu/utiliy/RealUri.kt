package com.juniorboom.hongmu.utiliy

data class RealUri(var path: String = "",
                   var isTempFile: Boolean = false)