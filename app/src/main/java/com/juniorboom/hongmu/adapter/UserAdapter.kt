package com.juniorboom.hongmu.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.juniorboom.hongmu.R
import com.juniorboom.hongmu.user.User
import kotlinx.android.synthetic.main.list_item_name_user.view.*

class UserAdapter : RecyclerView.Adapter<UserAdapter.ViewHolder>() {
    var list: List<User> = emptyList()
    var onItemClick: ((building: User) -> Unit)? = null


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder = ViewHolder.create(parent)
    override fun getItemCount(): Int = list.size
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(list[position], onItemClick)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        companion object {
            fun create(parent: ViewGroup): ViewHolder = ViewHolder(
                LayoutInflater.from(parent.context!!)
                    .inflate(R.layout.list_item_name_user, parent, false)
            )
        }

        fun bind(user: User, onItemClick: ((uer: User) -> Unit)?) {
           user.let {
                itemView.list_item_user.setOnClickListener {
                    onItemClick?.invoke(user)
                }
            }

            itemView.tv_title.text = user.name

            Glide.with(itemView.img_list_item)
                .load(user.image)
                .centerCrop()
                .into(itemView.img_list_item)
        }
    }
}