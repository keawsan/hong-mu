package com.juniorboom.ksu.extension

import android.graphics.drawable.Drawable
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.jozzee.android.core.getAppCompatActivity
import com.jozzee.android.core.resource.getColor
import com.juniorboom.hongmu.R

fun Fragment.getToolbar(): ActionBar? = getAppCompatActivity().supportActionBar

fun Fragment.initialToolbar(toolbar: Toolbar,
                            title: String? = null,
                            @ColorRes titleColor: Int? = null,
                            homeButtonIndicator: Drawable? = null) {
    getAppCompatActivity().setSupportActionBar(toolbar)
    setToolbarTitle(title, toolbar?.findViewById(R.id.toolbar_title), titleColor)
    setToolbarHomeButton(homeButtonIndicator)
}

fun Fragment.setToolbarHomeButton(homeButtonIndicator: Drawable? = null) = getToolbar()?.let { toolbar ->
    toolbar.setHomeButtonEnabled(homeButtonIndicator != null)
    toolbar.setDisplayHomeAsUpEnabled(homeButtonIndicator != null)
    toolbar.setHomeAsUpIndicator(homeButtonIndicator)
}


/**
 * Support custom title such as toolbar that have a center title.
 * but if not have custom title will be use title from action bar.
 */
fun Fragment.setToolbarTitle(title: String?,
                             tvTitle: TextView? = null,
                             @ColorRes colorTitle: Int? = null) = getToolbar()?.let { toolbar ->

    when (tvTitle != null) {
        true -> {
            toolbar.setDisplayShowTitleEnabled(false)
            toolbar.title = ""
            tvTitle.text = title ?: ""
            colorTitle?.let {
                tvTitle.setTextColor(getColor(colorTitle))
            }
        }
        false -> {
            toolbar.setDisplayShowTitleEnabled(title != null)
            toolbar.title = title ?: ""
        }
    }
}