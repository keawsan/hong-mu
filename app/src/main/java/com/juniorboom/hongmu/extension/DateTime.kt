package com.juniorboom.ksu.extension

import android.view.Display
import java.text.SimpleDateFormat
import java.util.*

const val DATE_TIME_API_FORMAT = "yyyy-MM-dd'T'HH:mm:ss"
const val DATE_TIME_DISPLAY_FORMAT = "dd/MM/yyyy HH:mm"

fun String.changeDateFormat(fromFormat : String,toFormat: String):String = try{
    val date = SimpleDateFormat(fromFormat, Locale.US).parse(this)
    SimpleDateFormat(toFormat, Locale.US).format(date)
}catch(e:Exception){
    this
}