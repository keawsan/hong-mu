package com.juniorboom.hongmu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.jozzee.android.core.fragment.FragmentContainer
import com.jozzee.android.core.fragment.replaceFragment
import com.juniorboom.hongmu.ui.ScreenSplash

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FragmentContainer.id = R.id.mainFragmentContainer
        replaceFragment(ScreenSplash(), clearStack = true, addToBackStack = false)
    }
}
