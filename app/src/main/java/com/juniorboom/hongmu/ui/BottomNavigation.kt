package com.juniorboom.hongmu.ui


import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.jozzee.android.core.fragment.replaceChildFragment

import com.juniorboom.hongmu.R
import kotlinx.android.synthetic.main.bottom_navigation.*

/**
 * A simple [Fragment] subclass.
 */
class BottomNavigation : Fragment() {

    private var cuurntItem: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.bottom_navigation, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_nev_menuBar.selectedItemId = if (cuurntItem != 0) cuurntItem else R.id.home_screen
        replaceScreen(btn_nev_menuBar.selectedItemId)
        subscribeUi()

    }

    private fun setupComponents() {
        btn_nev_menuBar.selectedItemId = btn_nev_menuBar.selectedItemId
        replaceScreen(btn_nev_menuBar.selectedItemId)
    }

    private fun subscribeUi() {
        btn_nev_menuBar.setOnNavigationItemSelectedListener { menuItem ->
            if (menuItem.itemId != btn_nev_menuBar.selectedItemId) {
                cuurntItem = menuItem.itemId
                replaceScreen(cuurntItem)
                true
            } else {
                false
            }
        }
    }

    fun replaceScreen(itemId: Int) {
        Log.e("From", "id:$itemId")
        when (itemId) {
            R.id.web_view_screen -> {
                Log.e("1", "navigation_screen + $itemId")
                Toast.makeText(context, "Website Hong Mu ....", Toast.LENGTH_SHORT).show()
            }
            R.id.person_screen -> {
                replaceChildFragment(R.id.navigation_host_fragment, ScreenProfile())
            }
            else -> {
                Log.e("4", "Home_screen + $itemId")
                replaceChildFragment(R.id.navigation_host_fragment, ScreenHome())
            }
        }
    }
}
