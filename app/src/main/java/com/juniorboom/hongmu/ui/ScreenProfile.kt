package com.juniorboom.hongmu.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toolbar

import com.juniorboom.hongmu.R
import com.juniorboom.hongmu.user.User
import com.juniorboom.ksu.extension.initialToolbar
import kotlinx.android.synthetic.main.toolbar.*

/**
 * A simple [Fragment] subclass.
 */
class ScreenProfile : Fragment() {

    private var userAccount: User? = null

    companion object {
        const val KEY_BUILDING = "users"
        fun newInstance(user: User) = ScreenProfile().apply {
            arguments = Bundle().apply {
                putParcelable("user",user)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        setHasOptionsMenu(true)

        arguments?.let {
            userAccount = it.getParcelable(KEY_BUILDING)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.screen_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialToolbar(toolbar!!,getString(R.string.profile), R.color.whilt)
    }

}
