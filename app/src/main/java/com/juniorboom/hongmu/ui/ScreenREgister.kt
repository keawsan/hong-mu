package com.juniorboom.hongmu.ui


import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log

import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.jozzee.android.core.connection.NetworkMonitor
import com.jozzee.android.core.format.isEmail
import com.jozzee.android.core.fragment.replaceFragment
import com.jozzee.android.core.ui.content
import com.jozzee.android.core.ui.showToast
import com.juniorboom.hongmu.R
import com.juniorboom.hongmu.user.User
import com.juniorboom.hongmu.utiliy.ImageUtil
import com.juniorboom.hongmu.utiliy.RealUri
import com.juniorboom.hongmu.utiliy.getRealPath
import com.juniorboom.ksu.extension.initialToolbar
import kotlinx.android.synthetic.main.screen_register.*
import kotlinx.android.synthetic.main.toolbar.*
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class ScreenREgister : Fragment() {

    private var uri: Uri? = null
    private var uriUtil: RealUri? = null
    private var imageUtil: ByteArray? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.screen_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialToolbar(toolbar!!, getString(R.string.app_name), R.color.whilt)

//        subscribeUi()

    }

    private fun setupComponents() {

    }

//    private fun subscribeUi() {
//        img_photo?.setOnClickListener {
//            Log.e("imgPhoto","photo")
//            selectPhoto()
//        }
//        btn_register_save.setOnClickListener {
//            onCreateUser()
//        }
//    }
//
//    private fun onCreateUser() {
//        val email = edt_email.content()
//        val password = edt_password.content()
//        val confirm = edt_confirm_password.content()
//        if (email.isEmpty() && password.isEmpty()) {
//            return Toast.makeText(context, getString(R.string.app_name), Toast.LENGTH_SHORT).show()
//        } else if (password.isEmpty()) {
//            return Toast.makeText(context, getString(R.string.app_name), Toast.LENGTH_SHORT).show()
//        } else if (confirm.isEmpty()) {
//            return Toast.makeText(context, getString(R.string.app_name), Toast.LENGTH_SHORT).show()
//        } else if (password.length < 6) {
//            return Toast.makeText(context, getString(R.string.app_name), Toast.LENGTH_SHORT).show()
//        } else if (email.isEmpty()) {
//            return Toast.makeText(context, getString(R.string.app_name), Toast.LENGTH_SHORT).show()
//        } else if (!email.isEmail()) {
//            return Toast.makeText(context, getString(R.string.app_name), Toast.LENGTH_SHORT).show()
//        } else if (uri == null) {
//            return Toast.makeText(context, getString(R.string.app_name), Toast.LENGTH_SHORT).show()
//        } else if (password != confirm) {
//            return Toast.makeText(context, getString(R.string.app_name), Toast.LENGTH_SHORT).show()
//        }
//        if (password == confirm && uri != null && email.isEmail() && password.length >= 6) {
//            Log.e("Register", "Complete")
//            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
//                .addOnCompleteListener {
//                    if (it.isSuccessful) {
//                        Log.e("Register", "uid : ${it.result?.user?.uid}")
//                        upLoadPhoto()
//                        sentVerification()
//                        dialogRegister()
////                            replaceFragment(LoginScreen(), clearStack = true, addToBackStack = false)
//                    } else {
//                        Toast.makeText(
//                            context,
//                            getString(R.string.app_name),
//                            Toast.LENGTH_SHORT
//                        ).show()
//                    }
//                }
//        }
//    }
//
//    private fun selectPhoto() {
//        try {
//            if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
//            ) {
//                ActivityCompat.requestPermissions(activity!!, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE
//                    ), 0
//                )
//            } else {
//                val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
//                startActivityForResult(galleryIntent, 0)
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//    }
//
//    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
//        if (requestCode == 0 && resultCode == Activity.RESULT_OK && data != null) {
//            uri = data.data
//            uriUtil = data.data?.getRealPath(context)
//            imageUtil = ImageUtil()
//                .reduceImageSizeBeforeUpload(uriUtil?.path.toString(), 400)
//            //            btn_img_upLoad.setImageURI(uri)
//            Glide.with(img_photo)
//                .load(imageUtil)
//                .circleCrop()
//                .into(img_photo)
//                .clearOnDetach()
//        }
//    }
//
//    private fun upLoadPhoto() {
//        if (imageUtil == null) return
//        val filename = UUID.randomUUID().toString()
//        val ref = FirebaseStorage.getInstance().getReference("/images/$filename")
//        ref.putBytes(imageUtil!!).addOnSuccessListener { it ->
//            Log.d("Register", "image metadata : ${it.metadata?.path}")
//            ref.downloadUrl.addOnSuccessListener {
//                Log.d("Register", "File Location URL : $it")
//                saveUserToDatabase(it.toString(), filename)
//            }
//        }
//    }
//
//    private fun saveUserToDatabase(profileImage: String, nameImage: String) {
//        val user = FirebaseAuth.getInstance().currentUser
//        val uid = user?.uid
//        val email = user?.email
//        val ref = FirebaseFirestore.getInstance()
//        val getUser = User(
//            "",
//            "",
//            email.toString(),
//            "",
//            profileImage,
//            "",
//            nameImage
//        )
//        ref.collection("User").document(uid.toString()).set(getUser).addOnSuccessListener {
//            Log.d("Register", "UpLoad Profile Success")
//        }
//
//    }
//
//    private fun sentVerification() {
//        if (NetworkMonitor.isConnected) {
//            FirebaseAuth.getInstance().currentUser?.sendEmailVerification()?.addOnCompleteListener {
//                if (it.isSuccessful) {
//                    FirebaseAuth.getInstance().signOut()
//                    Log.w("Email", "Complete")
//                }
//            }
//        } else {
//            showToast(R.string.not_connect_internet)
//        }
//    }
//
//    private fun dialogRegister() = androidx.appcompat.app.AlertDialog.Builder(context!!).apply {
//        setTitle(getString(R.string.register_completed))
//        setMessage(getString(R.string.text_register_signIn))
//        setPositiveButton(getString(R.string.confirm)) { dialog, _ ->
//            if (NetworkMonitor.isConnected) {
//                dialog.dismiss()
//                replaceFragment(ScreenSingIn(), clearStack = true, addToBackStack = false)
//            } else {
//                showToast(R.string.not_connect_internet)
//            }
//        }
//    }.create().show()


}
