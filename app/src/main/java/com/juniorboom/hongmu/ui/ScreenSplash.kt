package com.juniorboom.hongmu.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jozzee.android.core.fragment.replaceFragment

import com.juniorboom.hongmu.R
import com.juniorboom.hongmu.utiliy.runOnMainThread

/**
 * A simple [Fragment] subclass.
 */
class ScreenSplash : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.screen_splash, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        runOnMainThread(delay = 3000) {
            replaceFragment(BottomNavigation(), clearStack = true, addToBackStack = false)
        }
    }
}
