package com.juniorboom.hongmu.ui


import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.commit
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.FirebaseFirestore

import com.juniorboom.hongmu.R
import com.juniorboom.hongmu.adapter.UserAdapter
import kotlinx.android.synthetic.main.screen_home.*

/**
 * A simple [Fragment] subclass.
 */
class ScreenHome : Fragment() {

    private lateinit var adapter: UserAdapter
    private var userAccount : ArrayList<com.juniorboom.hongmu.user.User> = ArrayList<com.juniorboom.hongmu.user.User>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.screen_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Setup components
        adapter = UserAdapter()
        adapter.onItemClick = ::onItemClick
        recycler_view.layoutManager = LinearLayoutManager(context!!)
        recycler_view.adapter = adapter

        loadFeedBuilding()
        progress_bar.visibility = View.VISIBLE

    }
    private fun loadFeedBuilding() {

        val db = FirebaseFirestore.getInstance().collection("users")
        db.get()
            .addOnSuccessListener { result ->
                progress_bar?.visibility = View.INVISIBLE
                //Log.d("Resultl", "${result.documents}}")
                for (document in result) {

                    val userAccounts = document.toObject(com.juniorboom.hongmu.user.User::class.java)
                    userAccount.add(userAccounts)
                }
                adapter.list = userAccount
                adapter.notifyDataSetChanged()

            }
            .addOnFailureListener {
                Log.i(ContentValues.TAG, "Error getting documents")
            }

    }

    private fun onItemClick(user: com.juniorboom.hongmu.user.User){
        //On click news
        Toast.makeText(context,"", Toast.LENGTH_LONG).show()
        activity!!.supportFragmentManager.commit {
            addToBackStack("")
            replace(R.id.mainFragmentContainer, ScreenProfile.newInstance(user),"userAccount")


        }
    }

}
